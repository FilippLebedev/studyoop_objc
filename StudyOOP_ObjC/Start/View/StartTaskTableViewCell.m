//
//  StartTaskTableViewCell.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import "StartTaskTableViewCell.h"

@implementation StartTaskTableViewCell

-(NSString *)title {
    return _labelTitle.text;
}

-(void)setTitle: (NSString *)newValue {
    _labelTitle.text = newValue;
}

@end
