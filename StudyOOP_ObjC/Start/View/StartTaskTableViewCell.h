//
//  StartTaskTableViewCell.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StartTaskTableViewCell : UITableViewCell {
    NSString *title;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

-(NSString *)title;
-(void)setTitle: (NSString *)newValue;

@end

NS_ASSUME_NONNULL_END
