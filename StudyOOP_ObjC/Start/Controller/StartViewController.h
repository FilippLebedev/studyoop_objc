//
//  StartViewController.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StartViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
