//
//  StartViewController.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import "StartViewController.h"
#import "StartTaskTableViewCell.h"
#import "Manager.h"

@interface StartViewController ()
{
    NSArray* tasks;
}

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tasks = @[@"WorkTime"];
    
    self.table.delegate = self;
    self.table.dataSource = self;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellTask";
    StartTaskTableViewCell *cell = (StartTaskTableViewCell *)[self.table dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.title = tasks[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tasks.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self taskSelected:indexPath.row];
}

- (void)taskSelected: (int)task {
    //[Manager.shared.router openTask:@"WorkTime"];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WorkTimeViewController" bundle:nil];
    WorkTimeViewController *view = [sb instantiateViewControllerWithIdentifier:@"WorkTimeViewController"];
    [self presentViewController:view animated:YES completion:nil];
}

@end
