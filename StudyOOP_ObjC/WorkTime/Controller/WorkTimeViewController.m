//
//  WorkTimeViewController.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WorkTimeViewController.h"
#import "WorkTimeDataSource.h"
#import "WorkTimeRecordGroup.h"
#import "WorkTimeIntervalRangeTemplate.h"
#import "WorkTimeIntervalRange.h"
#import "WorkTimeRecord.h"
#import "WorkTimeCalendarView.h"
#import "WorkTimeRecordGroupManager.h"

@implementation WorkTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self prepareDataSource];
    [self prepareCalendarView];

    [self viewIsReady];
    
    [self setConstraints];
}

- (void)prepareDataSource {
    self.dataSource = [WorkTimeDataSource new];
    [_dataSource setDateFormat: @"E"];
}

- (void)prepareCalendarView {
    _viewCalendar = [[WorkTimeCalendarView alloc] initWithFrame:CGRectNull];
    
    [self.view addSubview: self.viewCalendar];
    
    self.viewCalendar.delegate = self;
}

- (void)update: (NSMutableArray*)records {
    [_groupManager update: records];
    [_dataSource update: _groupManager.groups];
    [_viewCalendar reloadData];
}

- (int)numberOfIndicators {
    return _dataSource.intervalQuantity;
}

- (NSTimeInterval)value: (int)indicator {
    return [_dataSource timeAmountForInterval:indicator];
}

- (NSTimeInterval)maximumValue: (int)indicator {
    return [_dataSource maximalTimeAmountForInterval: indicator];
}

- (NSString *)labelText: (int)indicator {
    return [_dataSource representativeDateForInterval: indicator];
}

- (void)viewIsReady {
    WorkTimeIntervalRangeTemplate* intervalRangeTemplate = [[WorkTimeIntervalRangeTemplate alloc] initWithIntervalDuration:86400 :7];
    NSDate* startDate = [[[NSDate alloc] init] dateByAddingTimeInterval:-86400 * 7];
    WorkTimeIntervalRange *intervalRange = [[WorkTimeIntervalRange alloc] initWithTemplate:intervalRangeTemplate :startDate];
    _groupManager = [[WorkTimeRecordGroupManager alloc] initWithRange:intervalRange];
    
    [self setup];
}

- (void)setup {
    
    // Data for example
    
    UInt32 timeRange = 86400 * 7;
    
    NSMutableArray* records = [[NSMutableArray alloc] init];
    
    for (int i = 0; i <= 50; ++i) {
        NSTimeInterval randomDateDifference = arc4random_uniform(timeRange);
        NSTimeInterval randomDuration = arc4random_uniform(7200);
        NSDate* startTime = [[[NSDate alloc] init] dateByAddingTimeInterval: -randomDateDifference];
        WorkTimeRecord* newRecord = [[WorkTimeRecord alloc] init: startTime : randomDuration];
        [records addObject:newRecord];
    }
    
    [self update:records];
}

- (void)setConstraints {
    [self.viewCalendar setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem: self.viewCalendar attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem: self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem: self.viewCalendar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem: self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem: self.viewCalendar attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem: self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem: self.viewCalendar attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem: self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    [NSLayoutConstraint activateConstraints:@[left, top, right, bottom]];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [_viewCalendar reloadData];
}

@end
