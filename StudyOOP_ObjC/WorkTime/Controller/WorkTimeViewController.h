//
//  WorkTimeViewController.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WorkTimeDataSource.h"
#import "WorkTimeRecordGroup.h"
#import "WorkTimeIntervalRangeTemplate.h"
#import "WorkTimeIntervalRange.h"
#import "WorkTimeRecord.h"
#import "WorkTimeCalendarView.h"
#import "WorkTimeRecordGroupManager.h"
#import "WorkTimeCalendarDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkTimeViewController : UIViewController <WorkTimeCalendarDelegate>

@property (nonatomic, strong) WorkTimeDataSource *dataSource;
@property (nonatomic, strong) WorkTimeCalendarView *viewCalendar;
@property (nonatomic, strong) WorkTimeRecordGroupManager *groupManager;

- (void)viewIsReady;
- (void)update: (NSMutableArray*)records;
- (void)setup;

- (void)prepareDataSource;
- (void)prepareCalendarView;
- (void)prepareDataSource: (NSMutableArray*)records;
- (int)numberOfIndicators;
- (NSTimeInterval)value: (int)indicator;
- (NSTimeInterval)maximumValue: (int)indicator;
- (NSString *)labelText: (int)indicator;
- (void)setConstraints;

@end

NS_ASSUME_NONNULL_END
