//
//  WorkTimeIndicatorView.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeIndicatorView_h
#define WorkTimeIndicatorView_h

#import <UIKit/UIKit.h>

@interface WorkTimeIndicatorView : UIView

@property (nonatomic) NSTimeInterval timeValue;
@property (nonatomic) NSTimeInterval maxTimeValue;
@property (nonatomic) UIColor* lineColor;
@property (nonatomic) UIColor* containerColor;
@property (nonatomic) float containerHeight;

@property (nonatomic, strong) UIView* viewContainer;
@property (nonatomic, strong) UIView* viewLine;
@property (nonatomic) NSLayoutConstraint* constraintLineHeight;

- (id)init: (CGRect)frame: (NSTimeInterval)time: (NSTimeInterval)maxTime;
- (void)setTime: (NSTimeInterval)value;
- (void)setMaxTime: (NSTimeInterval)value;
- (void)setLineColor: (UIColor*)color;
- (void)drawInitial;
- (void)updateView;
- (void)updateColors;

@end

#endif /* WorkTimeIndicatorView_h */
