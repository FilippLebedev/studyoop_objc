//
//  WorkTimeIndicatorView.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "WorkTimeIndicatorView.h"

@implementation WorkTimeIndicatorView

- (id)initWithFrame:(CGRect)aRect {
    self = [super initWithFrame:aRect];
    [self drawInitial];
    return self;
}

- (id)init: (CGRect)frame: (NSTimeInterval)time: (NSTimeInterval)maxTime {
    self = [super initWithFrame: frame];
    [self drawInitial];
    [self setTime: time];
    [self setMaxTime: maxTime];
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    return self;
}

- (void)setTime: (NSTimeInterval)value {
    _timeValue = value;
    [self updateView];
}

- (void)setMaxTime: (NSTimeInterval)value {
    _maxTimeValue = value;
    [self updateView];
}

- (void)setLineColor: (UIColor*)color {
    _lineColor = color;
    [self updateColors];
}

- (void)drawInitial {
    
    _viewContainer = [[UIView alloc] initWithFrame:CGRectNull];
    _viewLine = [[UIView alloc] initWithFrame:CGRectNull];
    
    [self addSubview: _viewContainer];
    [_viewContainer addSubview: _viewLine];
    
    [_viewContainer setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem: _viewContainer attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem: _viewContainer attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem: _viewContainer attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem: _viewContainer attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    [_viewLine setTranslatesAutoresizingMaskIntoConstraints: NO];

    _constraintLineHeight = [NSLayoutConstraint constraintWithItem: _viewLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_viewContainer attribute:NSLayoutAttributeHeight multiplier:0.5 constant:0];
    NSLayoutConstraint *leftLine = [NSLayoutConstraint constraintWithItem: _viewLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem: _viewContainer attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *rightLine = [NSLayoutConstraint constraintWithItem: _viewLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem: _viewContainer attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    NSLayoutConstraint *bottomLine = [NSLayoutConstraint constraintWithItem: _viewLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem: _viewContainer attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    [NSLayoutConstraint activateConstraints:@[left, top, right, bottom, leftLine, rightLine, bottomLine, _constraintLineHeight]];
    
    [self setLineColor: [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0]];
    _containerColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    [self updateColors];
}

- (void)updateView {
    if (_maxTimeValue > 0) {
        
        CGFloat multiplier = (1 / _maxTimeValue) * _timeValue;

        NSLayoutConstraint *oldConstraint = _constraintLineHeight;
        NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:oldConstraint.firstItem
                                                                         attribute:oldConstraint.firstAttribute
                                                                         relatedBy:oldConstraint.relation
                                                                            toItem:oldConstraint.secondItem
                                                                         attribute:oldConstraint.secondAttribute
                                                                        multiplier: multiplier
                                                                          constant:oldConstraint.constant];

        [NSLayoutConstraint deactivateConstraints:@[oldConstraint]];
        [NSLayoutConstraint activateConstraints:@[newConstraint]];
    }
}

- (void)updateColors {
    _viewLine.backgroundColor = _lineColor;
    _viewContainer.backgroundColor = _containerColor;
}

@end
