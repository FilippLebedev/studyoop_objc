//
//  WorkTimeCalendarView.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "WorkTimeCalendarView.h"
#import "WorkTimeIndicatorView.h"
#import "WorkTimeDateLabel.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

@implementation WorkTimeCalendarView

- (void)reloadData {
    [self reset];
    
    const numberOfIndicators = [_delegate numberOfIndicators];
    
    for (int i = 0; i < numberOfIndicators; ++i) {
        NSTimeInterval indicatorValue = [_delegate value: i];
        NSTimeInterval maxValue = [_delegate maximumValue: i];
        WorkTimeIndicatorView* newIndicator = [[WorkTimeIndicatorView alloc] init: CGRectNull : indicatorValue : maxValue];
        [_indicators addObject:newIndicator];
        
        NSString* labelText = [_delegate labelText:i];
        WorkTimeDateLabel* newLabel = [[WorkTimeDateLabel alloc] initWithText:labelText];
        [_labels addObject:newLabel];
    }
    
    [self drawIndicators];
    [self drawLabels];
}

- (void)drawIndicators {
    NSUInteger index = 0;
    
    for (WorkTimeIndicatorView* indicator in _indicators) {
        [self addSubview: indicator];

        CGFloat screenWidth = 0.0;

        if (UIInterfaceOrientationIsPortrait([UIDevice currentDevice].orientation)) {
            screenWidth = SCREEN_WIDTH;
        } else {
            screenWidth = SCREEN_HEIGHT;
        }
        
        CGFloat maximalWidthForInterval =  screenWidth / 7;
        CGFloat leftMargin = 20 + maximalWidthForInterval * index;
        
        [indicator setTranslatesAutoresizingMaskIntoConstraints: NO];
        
        NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem: indicator attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeLeft multiplier:1 constant: leftMargin];
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem: indicator attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeTop multiplier:1 constant:50];
        NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem: indicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem: nil attribute:NSLayoutAttributeWidth multiplier:1 constant:20];
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem: indicator attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem: self attribute:NSLayoutAttributeBottom multiplier:1 constant:-50];

        [NSLayoutConstraint activateConstraints:@[left, bottom, top, width]];
        
        [self layoutIfNeeded];
        
        index++;
    }
}

- (void)drawLabels {
    NSUInteger index = 0;
    
    for (id label in _labels) {
        [self addSubview: label];
        
        if (index < _indicators.count) {
            WorkTimeIndicatorView* indicator = _indicators[index];
            
            [label setTranslatesAutoresizingMaskIntoConstraints: NO];
            
            NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem: label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem: indicator attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
            NSLayoutConstraint *center = [NSLayoutConstraint constraintWithItem: label attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem: indicator attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];

            [NSLayoutConstraint activateConstraints:@[top, center]];
        }
        
        index++;
    }
}

- (void)reset {
    _indicators = [[NSMutableArray alloc] init];
    _labels = [[NSMutableArray alloc] init];
    [self clearView];
}

- (void)clearView {
    for (id s in self.subviews) {
        [s removeFromSuperview];
    }
}

@end
