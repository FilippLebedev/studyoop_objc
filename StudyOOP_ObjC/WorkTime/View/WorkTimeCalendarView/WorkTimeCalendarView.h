//
//  WorkTimeCalendarView.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeCalendarView_h
#define WorkTimeCalendarView_h

#import <UIKit/UIKit.h>

#import "WorkTimeCalendarDelegate.h"

@interface WorkTimeCalendarView : UIView

@property (nonatomic, weak) id <WorkTimeCalendarDelegate> delegate;
@property (nonatomic, strong) NSMutableArray* indicators;
@property (nonatomic, strong) NSMutableArray* labels;

- (void)reloadData;
- (void)drawIndicators;
- (void)drawLabels;
- (void)reset;
- (void)clearView;

@end

#endif /* WorkTimeCalendarView_h */
