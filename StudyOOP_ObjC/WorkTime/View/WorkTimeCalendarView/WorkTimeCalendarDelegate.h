//
//  WorkTimeCalendarDelegate.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol WorkTimeCalendarDelegate <NSObject>

- (int)numberOfIndicators;
- (NSTimeInterval)value: (int)indicator;
- (NSTimeInterval)maximumValue: (int)indicator;
- (NSString*)labelText: (int)indicator;

@end

NS_ASSUME_NONNULL_END
