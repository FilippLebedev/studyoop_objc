//
//  WorkTimeDateLabel.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeDateLabel_h
#define WorkTimeDateLabel_h

#import <UIKit/UIKit.h>

@interface WorkTimeDateLabel : UILabel

- (instancetype)initWithText: (NSString*)text;

@end

#endif /* WorkTimeDateLabel_h */
