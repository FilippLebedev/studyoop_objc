//
//  WorkTimeDateLabel.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "WorkTimeDateLabel.h"

@implementation WorkTimeDateLabel

- (instancetype)initWithText: (NSString*)text {
    self = [super initWithFrame: CGRectNull];
    self.textAlignment = NSTextAlignmentCenter;
    self.text = text;
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    return self;
}

@end
