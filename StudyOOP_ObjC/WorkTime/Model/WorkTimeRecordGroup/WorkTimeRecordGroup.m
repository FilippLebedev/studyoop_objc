//
//  WorkTimeRecordGroup.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeRecordGroup.h"

@implementation WorkTimeRecordGroup

- (instancetype)init: (WorkTimeInterval*)interval {
    if (self = [super init]) {
        _interval = interval;
    }
    if (_records == nil) {
        _records = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSTimeInterval)timeAmount {
    NSTimeInterval fullTime = 0.0;
    
    for (id record in _records) {
        NSTimeInterval time = [record duration];
        fullTime += time;
    }
    
    return fullTime;
}

- (void)appendIfIncludeInterval: (WorkTimeRecord *)record {
    
    NSComparisonResult comparsionMin = [record.startTime compare:_interval.minimalDate];
    NSComparisonResult comparsionMax = [record.startTime compare:_interval.maximalDate];
    
    if (comparsionMin == NSOrderedDescending  && comparsionMax == NSOrderedAscending) {
        [_records addObject:record];
    }
    
//    if (record.startTime >= _interval.minimalDate && record.startTime <= _interval.maximalDate) {
//        [_records addObject:record];
//    }
}

@end
