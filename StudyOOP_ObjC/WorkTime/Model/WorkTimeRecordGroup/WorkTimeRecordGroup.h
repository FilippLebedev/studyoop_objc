//
//  WorkTimeRecordGroup.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeRecordGroup_h
#define WorkTimeRecordGroup_h

#import "WorkTimeRecord.h"
#import "WorkTimeInterval.h"

@interface WorkTimeRecordGroup : NSObject

@property (nonatomic, strong) NSMutableArray *records;
@property (nonatomic, strong) WorkTimeInterval *interval;

- (instancetype)init: (WorkTimeInterval*)interval;
- (NSTimeInterval)timeAmount;
- (void)appendIfIncludeInterval: (WorkTimeRecord *)record;

@end

#endif /* WorkTimeRecordGroup_h */
