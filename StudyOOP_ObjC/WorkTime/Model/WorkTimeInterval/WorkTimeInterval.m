//
//  WorkTimeInterval.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeInterval.h"

@implementation WorkTimeInterval

- (instancetype)initWithDateRange: (NSDate*)minimalDate: (NSDate*)maximalDate {
    if (self = [super init]) {
        _minimalDate = minimalDate;
        _maximalDate = maximalDate;
    }
    return self;
}

- (NSTimeInterval)maximalDuration {
    return [_maximalDate timeIntervalSinceDate: _minimalDate];
}

@end
