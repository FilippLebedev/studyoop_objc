//
//  WorkTimeInterval.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeInterval_h
#define WorkTimeInterval_h

@interface WorkTimeInterval : NSObject

@property (nonatomic, strong) NSDate* minimalDate;
@property (nonatomic, strong) NSDate* maximalDate;

- (instancetype)initWithDateRange: (NSDate*)minimalDate: (NSDate*)maximalDate ;
- (NSTimeInterval)maximalDuration;

@end

#endif /* WorkTimeInterval_h */
