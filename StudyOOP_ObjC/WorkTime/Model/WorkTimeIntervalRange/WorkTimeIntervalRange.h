//
//  WorkTimeIntervalRange.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeIntervalRange_h
#define WorkTimeIntervalRange_h

#import "WorkTimeIntervalRangeTemplate.h"

@interface WorkTimeIntervalRange : NSObject

@property (nonatomic, strong) NSMutableArray* intervals;

- (instancetype)initWithTemplate: (WorkTimeIntervalRangeTemplate*)template: (NSDate*)startTime;

@end

#endif /* WorkTimeIntervalRange_h */
