//
//  WorkTimeIntervalRange.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeIntervalRange.h"
#import "WorkTimeIntervalRangeTemplate.h"
#import "WorkTimeInterval.h"

@implementation WorkTimeIntervalRange

- (instancetype)initWithTemplate: (WorkTimeIntervalRangeTemplate*)template: (NSDate*)startTime {
    if (self = [super init]) {
        _intervals = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < template.intervalQuantity; ++i) {
            NSDate *minimalDate = [startTime dateByAddingTimeInterval: i * template.intervalDuration];
            NSDate *maximalDate = [minimalDate dateByAddingTimeInterval: template.intervalDuration];
            WorkTimeInterval *newInterval = [[WorkTimeInterval alloc] initWithDateRange: minimalDate : maximalDate];
            [_intervals addObject:newInterval];
        }
    }
    return self;
}

@end
