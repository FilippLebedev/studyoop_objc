//
//  WorkTimeRecord.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeRecord_h
#define WorkTimeRecord_h

#import <Foundation/Foundation.h>

@interface WorkTimeRecord : NSObject

@property (nonatomic) NSDate* startTime;
@property (nonatomic) NSTimeInterval duration;

-(instancetype)init: (NSDate*)startTime : (NSTimeInterval)duration;

@end

#endif /* WorkTimeRecord_h */
