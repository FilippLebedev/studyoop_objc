//
//  WorkTimeRecord.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeRecord.h"

@implementation WorkTimeRecord

-(instancetype)init: (NSDate*)startTime : (NSTimeInterval)duration {
    if (self = [super init]) {
        _startTime = startTime;
        _duration = duration;
    }
    return self;
}

@end
