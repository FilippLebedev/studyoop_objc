//
//  WorkTimeIntervalRangeTemplate.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeIntervalRangeTemplate.h"

@implementation WorkTimeIntervalRangeTemplate

- (instancetype)initWithIntervalDuration: (NSTimeInterval)intervalDuration: (int)intervalQuantity {
    if ((self = [super init])) {
        _intervalDuration = intervalDuration;
        _intervalQuantity = intervalQuantity;
    }
    return self;
}

@end
