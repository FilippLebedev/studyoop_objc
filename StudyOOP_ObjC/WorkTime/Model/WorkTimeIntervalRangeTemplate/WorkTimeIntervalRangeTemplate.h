//
//  WorkTimeIntervalRangeTemplate.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeIntervalRangeTemplate_h
#define WorkTimeIntervalRangeTemplate_h

@interface WorkTimeIntervalRangeTemplate : NSObject

@property (nonatomic) NSTimeInterval intervalDuration;
@property (nonatomic) int intervalQuantity;

- (instancetype)initWithIntervalDuration: (NSTimeInterval)intervalDuration: (int)intervalQuantity;

@end

#endif /* WorkTimeIntervalRangeTemplate_h */
