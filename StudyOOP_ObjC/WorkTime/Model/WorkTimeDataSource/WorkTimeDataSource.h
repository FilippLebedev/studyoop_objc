//
//  WorkTimeDataSource.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeDataSource_h
#define WorkTimeDataSource_h

#import "WorkTimeRecordGroupDateRepresentation.h"

@interface WorkTimeDataSource : NSObject

@property (nonatomic, strong) WorkTimeRecordGroupDateRepresentation* groupDateRepresentation;
@property (nonatomic, strong) NSMutableArray* data;

- (int)intervalQuantity;
- (NSTimeInterval)timeAmountForInterval: (int)index;
- (NSTimeInterval)maximalTimeAmountForInterval: (int)index;
- (NSDate*)minimalDateForInterval: (int)index;
- (NSString*)representativeDateForInterval: (int)index;
- (void)update: (NSMutableArray*)data;
- (void)setDateFormat: (NSString*)format;

@end

#endif /* WorkTimeDataSource_h */
