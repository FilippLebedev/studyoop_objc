//
//  WorkTimeDataSource.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 02.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WorkTimeDataSource.h"
#import "WorkTimeRecordGroupDateRepresentation.h"
#import "WorkTimeRecordGroup.h"

@implementation WorkTimeDataSource

- (int)intervalQuantity {
    return _data.count;
}

- (NSTimeInterval)timeAmountForInterval: (int)index {
    if (index >= 0 && index < [self intervalQuantity]) {
        return [_data[index] timeAmount];
    }
    return 0;
}

- (NSTimeInterval)maximalTimeAmountForInterval: (int)index {
    if (index >= 0 && index < [self intervalQuantity]) {
        WorkTimeRecordGroup* group = (WorkTimeRecordGroup*)_data[index];
        return [group.interval maximalDuration];
    }
    return 0;
}

- (NSDate*)minimalDateForInterval: (int)index {
    if (index >= 0 && index < [self intervalQuantity]) {
        WorkTimeRecordGroup* group = (WorkTimeRecordGroup*)_data[index];
        return [group.interval minimalDate];
    }
    return [NSDate alloc];
}

- (NSString*)representativeDateForInterval: (int)index {
    if (index >= 0 && index < [self intervalQuantity]) {
        return [_groupDateRepresentation representativeDate: [self minimalDateForInterval:index]];
    }
    return @"";
}

- (void)update: (NSMutableArray*)data {
    _data = data;
}

- (void)setDateFormat: (NSString*)format {
    _groupDateRepresentation = [[WorkTimeRecordGroupDateRepresentation alloc] initWithDateFormat:format];
}

@end
