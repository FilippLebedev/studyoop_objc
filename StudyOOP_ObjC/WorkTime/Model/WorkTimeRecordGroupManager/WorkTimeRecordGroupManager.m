//
//  WorkTimeRecordGroupManager.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeRecordGroupManager.h"
#import "WorkTimeRecordGroup.h"

@implementation WorkTimeRecordGroupManager

- (instancetype)initWithRange: (WorkTimeIntervalRange*)range {
    if (self = [super init]) {
        _range = range;
    }
    return self;
}

- (NSTimeInterval)timeAmount {
    NSTimeInterval fullTime = 0.0;
    
    for (id group in _groups) {
        NSTimeInterval time = [group timeAmount];
        fullTime += time;
    }
    
    return fullTime;
}

- (void)update: (NSMutableArray*)records {
    _groups = [[NSMutableArray alloc] init];
    
    for (id interval in _range.intervals) {
        WorkTimeRecordGroup *newGroup = [[WorkTimeRecordGroup alloc] init:interval];
        
        for (id record in records) {
            [newGroup appendIfIncludeInterval:record];
        }
        [_groups addObject:newGroup];
    }
}

@end
