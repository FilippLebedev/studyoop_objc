//
//  WorkTimeRecordGroupManager.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeRecordGroupManager_h
#define WorkTimeRecordGroupManager_h

#import "WorkTimeIntervalRange.h"

@interface WorkTimeRecordGroupManager : NSObject

@property (nonatomic, strong) NSMutableArray* groups;
@property (nonatomic, strong) WorkTimeIntervalRange* range;

- (NSTimeInterval)timeAmount;
- (instancetype)initWithRange: (WorkTimeIntervalRange*)range;
//- (void)update: (NSMutableArray*)records : (void (^)(NSMutableArray*))completion;
- (void)update: (NSMutableArray*)records;

@end

#endif /* WorkTimeRecordGroupManager_h */
