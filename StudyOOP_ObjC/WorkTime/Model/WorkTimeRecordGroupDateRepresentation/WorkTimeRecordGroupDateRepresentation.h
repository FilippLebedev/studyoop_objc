//
//  WorkTimeRecordGroupDateRepresentation.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef WorkTimeRecordGroupDateRepresentation_h
#define WorkTimeRecordGroupDateRepresentation_h

@interface WorkTimeRecordGroupDateRepresentation : NSObject

@property (nonatomic, strong) NSDateFormatter* formatter;

- (instancetype)initWithDateFormat: (NSString *)dateFormat;
- (NSString*)representativeDate: (NSDate*)date;

@end

#endif /* WorkTimeRecordGroupDateRepresentation_h */
