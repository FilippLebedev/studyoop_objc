//
//  WorkTimeRecordGroupDateRepresentation.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 01.12.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkTimeRecordGroupDateRepresentation.h"

@implementation WorkTimeRecordGroupDateRepresentation

- (instancetype)initWithDateFormat: (NSString *)dateFormat {
    if (self = [super init]) {
        if (_formatter == nil) {
            _formatter = [[NSDateFormatter alloc] init];
        }
        _formatter.dateFormat = dateFormat;
    }
    return self;
}

- (NSString*)representativeDate: (NSDate*)date {
    return [_formatter stringFromDate:date];
}

@end
