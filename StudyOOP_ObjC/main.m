//
//  main.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
