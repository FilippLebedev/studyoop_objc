//
//  Router.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 28.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Manager.h"
#import "ModuleConfigurator.h"

@implementation Router: NSObject

-(void)openTask: (NSString*)name {
    UIViewController *module;
    
    if ([name isEqual: @"WorkTime"]) {
        module = [Manager.shared.moduleConfigurator createWorkTime];
    }
    
    [Manager.shared pushViewController: module];
}

@end
