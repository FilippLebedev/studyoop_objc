//
//  Router.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 28.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef Router_h
#define Router_h

@interface Router: NSObject

-(void)openTask: (NSString*)name;

@end

#endif /* Router_h */
