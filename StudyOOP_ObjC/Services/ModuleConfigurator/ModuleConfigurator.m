//
//  ModuleConfigurator.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StartViewController.h"
#import "WorkTimeViewController.h"

@implementation ModuleConfigurator: NSObject

-(StartViewController*)createStart {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"StartViewController" bundle:nil];
    StartViewController *view = [sb instantiateViewControllerWithIdentifier:@"StartViewController"];
    return view;
}

-(WorkTimeViewController*)createWorkTime {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WorkTimeViewController" bundle:nil];
    WorkTimeViewController *view = [sb instantiateViewControllerWithIdentifier:@"WorkTimeViewController"];
    return view;
}

@end
