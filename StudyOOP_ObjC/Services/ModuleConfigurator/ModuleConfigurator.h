//
//  ModuleConfigurator.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef ModuleConfigurator_h
#define ModuleConfigurator_h

#import "StartViewController.h"
#import "WorkTimeViewController.h"

@interface ModuleConfigurator: NSObject

-(StartViewController*)createStart;
-(WorkTimeViewController*)createWorkTime;

@end

#endif /* ModuleConfigurator_h */
