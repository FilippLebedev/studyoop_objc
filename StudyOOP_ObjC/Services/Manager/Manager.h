//
//  Manager.h
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#ifndef Manager_h
#define Manager_h

#import <UIKit/UIKit.h>
#import "ModuleConfigurator.h"
#import "Router.h"

@interface Manager: NSObject

+ (Manager*)shared;
- (void)configure;
- (void)pushViewController: (UIViewController*)vc;

@property (nonatomic, strong) ModuleConfigurator *moduleConfigurator;
@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) Router *router;

@end

#endif /* Manager_h */
