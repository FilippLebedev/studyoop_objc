//
//  Manager.m
//  StudyOOP_ObjC
//
//  Created by Filipp Lebedev on 20.11.2018.
//  Copyright © 2018 Filipp Lebedev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Manager.h"
#import "ModuleConfigurator.h"

@implementation Manager

+ (Manager*)shared {
    static Manager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (void)configure {
    _moduleConfigurator = [[ModuleConfigurator alloc] init];
    _router = [[Router alloc] init];
}

- (void)pushViewController: (UIViewController*)vc {
    [self.navigationController pushViewController:vc animated:YES];
}

@end

